<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%-- 欢迎 <shiro:guest>游客</shiro:guest><shiro:user><shiro:principal/></shiro:user>! &nbsp;    --%>
<shiro:user><a href="${pageContext.request.contextPath}/logout">退出</a></shiro:user>  
<shiro:guest><a href="${pageContext.request.contextPath}/login.jsp"><i class="glyphicon glyphicon-user"></i>登录</a></shiro:guest> 