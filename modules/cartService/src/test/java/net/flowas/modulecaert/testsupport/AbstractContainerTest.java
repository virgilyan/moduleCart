/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.flowas.modulecaert.testsupport;
import static org.junit.Assert.assertTrue;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;
import org.junit.BeforeClass;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

import java.net.BindException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

public abstract class AbstractContainerTest {
    public static final int MAX_PORT = 9200;

    protected static Server server;

    protected static int port = 9180;
    protected static  WebTarget webTarget ;
    
    private final static Client client = ClientBuilder.newClient();
    
    @BeforeClass
    public static void startContainer() throws Exception {
        while (server == null && port < MAX_PORT) {
            try {
                server = createAndStartServer(port);
            } catch (BindException e) {
                System.err.printf("Unable to listen on port %d.  Trying next port.", port);
                port++;
            }
        }
        assertTrue(server.isStarted());
    }
    
    private static Server createAndStartServer(final int port) throws Exception {
    	Server server = new Server(port);	    
    	WebAppContext war = new WebAppContext("src/test/resources/META-INF/webapp","/");
        war.setDescriptor("src/test/resources/META-INF/webapp/WEB-INF/web.xml");
    	server.setHandler(war );	       
        server.start();      
        webTarget = client.register(JacksonJsonProvider.class).target( getBaseUri());
        return server;
    }

    protected static String getBaseUri() {
    	// /rest/shop/
        return "http://localhost:" + port + "/";
    }
    
    public <T> T  getInterface(Class<T> clasz){
    	RemoteInvokeHandler.setBaseUrl(getBaseUri()+"rest/shop/");
    	T service = ClientFactory.getInstance().getInterface(clasz);
    	RemoteInvokeHandler.push(clasz);
    	return service;
    }
}
