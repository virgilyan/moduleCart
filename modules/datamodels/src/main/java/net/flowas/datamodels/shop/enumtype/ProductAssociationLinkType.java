/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flowas.datamodels.shop.enumtype;

/**
 *
 * @author liujh
 */
public enum ProductAssociationLinkType {
        /**
	 * 
	 */
	CROSS_SELLING("CROSS_SELLING"),
	
	/**
	 * 
	 */
	UP_SELLING("UP_SELLING"),
	
	/**
	 * 
	 */
	CUSTOMER_RECOMMENDATION("CUSTOMER_RECOMMENDATION");
	
	private String propertyKey = "";

	/**
	 * Constructor.
	 * 
	 * @param propertyKey the property key.
	 */
	ProductAssociationLinkType(final String propertyKey) {
		this.propertyKey = propertyKey;
	}

	/**
	 * Get the localization property key.
	 * 
	 * @return the localized property key
	 */
	public String getPropertyKey() {
		return this.propertyKey;
	}
}
