/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flowas.datamodels.shop;

import javax.persistence.Entity;
import javax.persistence.Lob;
import lombok.Data;

/**
 *
 * @author liujh
 */
@Data
@Entity
public class Retailer extends IdAndDate{
    private boolean active;
    private String code;
    private String name;
    @Lob
    private String description;
    private String logo;
    private boolean isOfficialRetailer;
    private boolean isBrand;
    private boolean isEcommerce;
    private boolean isCorner;
    private int qualityOfService;
    private int priceScore;
    private int ratioQualityPrice;
}
