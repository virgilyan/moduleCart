/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flowas.datamodels.shop;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Column;
import lombok.Data;

/**
 *
 * @author liujh
 */
@Data
@Entity
public class AttributeDefinition  extends IdAndDate{    
    private String code;
    private String name;
    @Lob
    private String description;
    private int attributeType;
    private int objectType;
    private boolean enabled;
    private boolean mandatory;
    private boolean localizable;
    @Column(name="reserved_global")
    private boolean global;
    private boolean multiValue;
    private boolean withPlanner;
    private Integer ordering;    
}
